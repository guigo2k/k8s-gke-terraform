# GCE project info
variable "project" {
  default = "symphony-gce-dev"
}

variable "credentials" {
  default = "account.json"
}

# GKE cluster config
variable "name" {
  default = "k8s-cluster"
}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "network" {
  default = "default"
}

variable "subnetwork" {
  default = "sym-infra-dev-guse1"
}

# Master auth.
variable "username" {
  default = "symphony"
}

variable "password" {
  default = "12358#"
}

# Nodes config
variable "initial_node_count" {
  default = "3"
}

# variable "image_type" {
#   default = ""
# }
#
# variable "machine_type" {
#   default = ""
# }
#
# variable "local_ssd_count" {
#   default = ""
# }
