provider "google" {
  credentials = "${file("${var.credentials}")}"
  project     = "${var.project}"
  region      = "${var.region}"
}

resource "google_container_cluster" "primary" {
  name = "${var.name}"
  zone = "${var.zone}"
  network = "${var.network}"
  subnetwork = "${var.subnetwork}"
  initial_node_count = "${var.initial_node_count}"

  master_auth {
    username = "${var.username}"
    password = "${var.password}"
  }

  node_config {
    # image_type = "${var.image_type}"
    # machine_type = "${var.machine_type}"
    # local_ssd_count = "${var.local_ssd_count}"
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring"
    ]
  }

  provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${var.name} --zone ${var.zone} --project ${var.project} && kubectl cluster-info"
  }
}
